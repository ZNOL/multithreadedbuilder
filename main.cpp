#include <iostream>
#include <functional>  // for std::function
#include <thread>
#include <set>

// https://yummy-buffer-177.notion.site/C-test-assignment-1444ab2ff23d45ae82b01dd63bd4ae1a

#define MAXSTEPS 1000000000000000

struct Target {
private:
    size_t id;
    bool build;
    std::function<void()> task;

public:
    Target() = default;
    Target(size_t _id, std::function<void()> _f) : id(_id),  build(false), task(std::move(_f)) {

    }
    Target(Target &old) : id(old.id), build(old.build), task(std::move(old.task)) {
    }

    Target &operator=(Target const &other) {
        if (this == &other) { return *this; }
        id = other.id;
        build = other.build;
        task = other.task;
        return *this;
    }

    bool isBuild() const { return build; }

    size_t getId() const { return id; }

    void doBuild() {
        if (!build) {
            build = true;
            // auto file = fopen(std::to_string(id).c_str(), "w");
            std::this_thread::sleep_for(std::chrono::seconds(1));  // for emulating long building
            // fclose(file);
            task();
        }
    }

    virtual ~Target() = default;
};

class BuildGraph {
public:
    std::unordered_map<size_t, Target> targets;
    std::unordered_map<size_t, std::vector<size_t>> gr;

    BuildGraph() = default;

    void input() {
        gr.clear();

        std::cout << "Input number of targets\nFor each target input target id\n\n";
        int n; std::cin >> n;
        for (int i = 0; i < n; ++i) {
            size_t id; std::cin >> id;

            std::function<void()> f = [&, id]() {
                std::cout << "BUILT id = " << id << std::endl;
            };
            Target tr(id, f);
            targets[id] = tr;
        }

        std::cout << "Input number of edges" << std::endl;
        int m; std::cin >> m;

        std::cout << "For each edge input thread and sub_thread\n\n";
        for (int i = 0; i < m; ++i) {
            size_t a, b; std::cin >> a >> b;

            if (gr.find(a) == gr.end()) {
                gr[a] = std::vector<size_t>();
            }
            gr[a].push_back(b);
        }

    }

    bool childCheck(const size_t target_id) {
        for (auto &to : gr[target_id]) {
            if (!targets[to].isBuild()) {
                return false;
            }
        }
        return true;
    }

    virtual ~BuildGraph() = default;
};

void myStaticBuilder(Target *t, size_t *cur_threads, const size_t *max_threads, BuildGraph *bg) {
    ++(*cur_threads);
    while (*cur_threads > *max_threads || !bg->childCheck(t->getId())) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    t->doBuild();
    --(*cur_threads);
}

class Builder {
private:
    bool builderror;
    std::unordered_map<size_t, int> used;
    std::vector<std::thread> threads;

    size_t cur_threads;
    size_t max_threads;

public:
    explicit Builder(size_t _num_threads) : builderror(false), cur_threads(0), max_threads(_num_threads) {}

    void execute(BuildGraph& build_graph, size_t target_id) {
        if (build_graph.targets.find(target_id) == build_graph.targets.end()) {
            throw std::invalid_argument("No such target");
        }

        builderror = false;
        cur_threads = 0;
        threads.clear();
        used.clear();

        std::vector<size_t> topsort;
        dfs(build_graph, target_id, topsort);

        if (builderror || topsort.empty()) {
            throw std::runtime_error("wrong build_graph");
        }

        size_t steps = 0;
        for (auto &cur_target : topsort) {
            while (true) {
                ++steps;
                if (steps > MAXSTEPS) {
                    throw std::runtime_error("Threads problem!");
                }

                if (cur_threads < max_threads) {
                    auto &t = build_graph.targets[cur_target];
                    std::thread thr(myStaticBuilder, &t, &cur_threads, &max_threads, &build_graph);
                    thr.detach();
                    break;
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }

        while (cur_threads != 0) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

    void dfs(BuildGraph &bg, size_t u, std::vector<size_t> &v) {
        if (builderror) {
            return;
        }

        used[u] = 1;
        for (auto &to : bg.gr[u]) {
            auto it = used.find(to);
            if (it == used.end()) {
                dfs(bg, to, v);
            } else if (it->second == 1) {
                builderror = true;
                return;
            }
        }

        used[u] = 2;
        v.push_back(u);
    }

    virtual ~Builder() = default;
};



int main() {
    BuildGraph bg;
    bg.input();

    size_t my_threads = 3;
    /*
    std::cout << "Input max number of threads << std::endl;
    std::cin > threads;
    */
    Builder builder(my_threads);

    size_t my_target = 1;
    /*
    std::cout << "Input target_id";
    std::cin >> my_target;
    */
    try {
        builder.execute(bg, my_target);
    }
    catch (const std::exception &exc) {
        std::cout << "Error!" << std::endl;
        std::cout << exc.what() << std::endl;
    }

    return 0;
}